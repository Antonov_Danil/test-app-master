import * as express from 'express';
import { Express } from 'express';
import * as bodyParser from 'body-parser';
import { Connection, getConnectionManager } from 'typeorm';
import typeORMConfig = require('../ormconfig');
import { UserController } from './Users/Users.controller';

export class App {
  public server: Express;
  public dbConnection: Connection;

  constructor() {
    this.server = express();
  }

  public async start() {
    await this.connectToDatabase();

    this.server.use(bodyParser.json({ limit: '50mb' }));
    this.server.use(bodyParser.text({ limit: '50mb' }));
    this.server.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));

    this.server.use(UserController);

    this.server.listen(3000, function() {
      console.log('Example app listening on port 3000!');
    });
  }

  private async connectToDatabase() {
    try {
      this.dbConnection = getConnectionManager().create(typeORMConfig);

      await this.dbConnection.connect();
      console.log('db good');
    } catch (err) {
      console.error(err);
    }
  }
}
