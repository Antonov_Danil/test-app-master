import * as jwt from 'jsonwebtoken';
import * as express from 'express';
import { User } from './Users.model';
import { validate } from 'class-validator';
import * as crypto from 'crypto';




const UserController = express.Router();


UserController.route('/users').post(async function(req, res, next) {
  try {
    const { email, password } = req.body;
    //const user = User.find({email,password});

    const user = User.create({ email, password});
    const token = jwt.sign({id:user.id},'supersecret',{expiresIn: 86400});
    user.token = token;
    const errors = await validate(user);

    if (errors.length) {
      const { constraints } = errors[0];
      return res.status(422).send({ details: constraints });
    }

    user.password = generatePassword(password);
    console.log(user);
    await user.save();

    return res.status(201).send({ id: user.id, email,token});
  } catch (err) {
    if (err.code === '23505') {
      return res.status(409).send({ details: err.detail });
    }
    console.log(err);
    return res.status(418).send(err);
  }
});


UserController.route('/login').post(async function (req, res, next) {



  // @ts-ignore
  const {email, password} = req.body;
  // @ts-ignore
  const user = await User.findOne({

    where: {email: email, password: generatePassword(password)}
  }).then(function (user){
    if(user!){

//jwt.sign(token, "your_jwt_secret");
      return res.status(200).send(user);
    }

return res.status(404).send(user);  }

  )
});
  // @ts-ignore
UserController.route('/me').get(async function (req,res,next) {

try {


  const token = req.header('x-access-token');
  if (!token) return res.status(401).send({auth: false, message: "No token provid"});
  const decoded = jwt.verify(token, "supersecret");
  return res.status(200).send(decoded);

}
catch (e) {
  return res.status(500).send({auth: false, message: "Failed to auth"});
}

  });

function generatePassword(password: string) {
  return crypto
    .createHash('sha256')
    .update(password)
    .digest('hex');
}


export { UserController };
