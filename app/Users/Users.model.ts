import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IsEmail, Length } from 'class-validator';
@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ unique: true, nullable: false })
  @IsEmail({})
  public email: string;

  @Column({ nullable: false })
  @Length(8, 20)
  public password: string;

  @Column()
  public token: string;
}
