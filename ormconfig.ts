import { ConnectionOptions } from 'typeorm';
import { User } from './app/Users/Users.model';

const typeORMConfig: ConnectionOptions = {
  type: 'postgres',
  host: '127.0.0.1',
  port: 5432,
  database: 'test',
  username: 'test',
  password: 'test',
  synchronize: true,
  logging: true,
  entities: [User]
};

export = typeORMConfig;
